<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <!-- Styles -->
    <link rel="stylesheet" href=" {{ asset('css/index.css') }} ">

    <title>TIS THE SEASON</title>
</head>

<body>
    <div class="container position-ref full-height">
        <div class="content">

            <form id="send-year-form">
                @csrf
                <div class="row mb-2">
                    <label for="year" class="col-form-label col-md-12">{{ __(' Enter a year') }}</label>
                </div>
                <div class="row mb-2">
                    <input id="year" type="number" name="year" class="form-control col-md-4 offset-md-4 text-center pr-0" name="year" value="{{ old('year') }}" required autocomplete="year" autofocus placeholder="example: 2021">
                </div>
                <div class="row mb-2">
                    <button type="submit" id="christmas-btn" class="btn py-1 px-2 my-1 col-md-4 offset-md-4">{{ __('Find Christmas') }}</button>
                </div>
            </form>
            <div>
                <table class="table">
                    <thead class="test-after row">
                        <tr class="col-md-6 offset-md-3">
                            <th scope="col" class="p-2 table-column">Year</th>
                            <th scope="col" class="p-2 table-column">Christmas Day</th>
                        </tr>
                    <tbody class="test-append">

                    </tbody>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="{{ asset('js/main.js') }}"></script>

</body>

</html>