<?php

namespace App\Http\Controllers;

use App\Date;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        return view('index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $year = $request->year;
        $arrayYears = [];
        $primeYears = [];

        for ($i = $request->year; count($primeYears) < 30; $i--) {
            if ($i % 2 != 0) {
                $primeYears[] = $i;
            }
        }

        foreach ($primeYears as $year) {
            $date = $year . '-01-07';
            $day = date('l', strtotime($date));

            $date = new Date();
            $date->year = $year;
            $date->christmas_day = $day;

            if(!Date::where('year', $year)->exists()){
            $date->save();
            }
           
            $data[] = $date;
        }
         
        return response()->json($data);
    }
}
