<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Date extends Model
{
    use Encryptable;

    public function toArray()
    {
        return [
            'year'   => $this->year,
            'christmas_day' => $this->christmas_day,
        ];
    }

    protected $fillable = [
        'year',
    ];

    protected $encryptable = [
        'christmas_day',
    ];
}
