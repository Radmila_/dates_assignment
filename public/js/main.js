$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('#send-year-form').on('submit', function(e) {
    e.preventDefault();
    let year = $('#year').val();
    $.ajax({
        url: "/send-year",
        type: "POST",
        data: {
            year: year,
        },
        success: function(response) {
            $('.test-append').empty();
            response.forEach(response => {
                $('.test-append').append(`
                <tr class="row">
                    <td scope="col" class="col-md-3 offset-md-3">${response.year}</td>
                    <td scope="col" class="col-md-3">${response.christmas_day}</td>
                </tr>
            `)
            })
        },
    });
});